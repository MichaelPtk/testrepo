package edu.bigdata.training.testservice.service;

import edu.bigdata.training.testservice.controller.model.Person;
import edu.bigdata.training.testservice.dao.TestServiceRepository;
import edu.bigdata.training.testservice.model.PersonEntity;

import java.util.List;
import java.util.UUID;

public class TestBusinessLogicService {

    private TestServiceRepository testServiceRepository;

    public TestBusinessLogicService(TestServiceRepository testServiceRepository) {
        this.testServiceRepository = testServiceRepository;
    }

    public PersonEntity processCreate(Person person){
        PersonEntity personEntity = new PersonEntity(person.getName());
        testServiceRepository.save(personEntity);
        return personEntity;
    }

    public PersonEntity processGet(String id){
        return testServiceRepository.get(UUID.fromString(id));
    }

    public PersonEntity processPut(String id, Person person){
        PersonEntity personEntity = new PersonEntity(person.getName());
        personEntity.setId(UUID.fromString(id));
        return testServiceRepository.put(personEntity);
    }

    public PersonEntity processDel(String id){
        testServiceRepository.del(UUID.fromString(id));

        return null;
    }

    public List<PersonEntity> processGetAll(){
        return testServiceRepository.getAll();
    }
}
